<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><p dir="auto"><a href="https://liberapay.com/FreshRSS/donate" rel="nofollow"><img src="https://camo.githubusercontent.com/c6c8d94c2140f4521aee4e3eb1b7500e6d1a01f6a1807f1b73b39643a5c792bf/68747470733a2f2f696d672e736869656c64732e696f2f6c69626572617061792f72656365697665732f46726573685253532e7376673f6c6f676f3d6c6962657261706179" alt="自由支付捐款" data-canonical-src="https://img.shields.io/liberapay/receives/FreshRSS.svg?logo=liberapay" style="max-width: 100%;"></a></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"></font><a href="https://github.com/FreshRSS/FreshRSS/blob/edge/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请在github.com/FreshRSS/FreshRSS/</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上阅读此文档</font><font style="vertical-align: inherit;">以获取正确的链接和图片。</font></font></li>
<li><a href="/FreshRSS/FreshRSS/blob/edge/README.fr.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法语版本</font></font></a></li>
</ul>
<h1 tabindex="-1" dir="auto"><a id="user-content-freshrss" class="anchor" aria-hidden="true" tabindex="-1" href="#freshrss"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新鲜RSS</font></font></h1>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FreshRSS 是一个自托管的 RSS 提要聚合器。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它重量轻、易于使用、功能强大且可定制。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它是一个具有匿名阅读模式的多用户应用程序。</font><font style="vertical-align: inherit;">它支持自定义标签。</font><font style="vertical-align: inherit;">有一个用于（移动）客户端的 API 和一个</font></font><a href="/FreshRSS/FreshRSS/blob/edge/cli/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">命令行界面</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">得益于</font></font><a href="https://freshrss.github.io/FreshRSS/en/users/WebSub.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WebSub</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">标准，FreshRSS 能够从兼容来源接收即时推送通知，例如</font></font><a href="https://friendi.ca" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Friendica</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><a href="https://wordpress.org/plugins/pubsubhubbub/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WordPress</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、Blogger、Medium 等。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FreshRSS 本身支持基于</font></font><a href="https://www.w3.org/TR/xpath-10/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">XPath 的</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基本 Web 抓取，适用于不提供任何 RSS / Atom 提要的网站。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持</font><font style="vertical-align: inherit;">不同的</font></font><a href="https://freshrss.github.io/FreshRSS/en/admins/09_AccessControl.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">登录方法：Web 表单（包括匿名选项）、HTTP 身份验证（与代理委托兼容）、OpenID Connect。</font></font></a><font style="vertical-align: inherit;"></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最后，FreshRSS 支持</font></font><a href="#extensions"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">扩展</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以进行进一步调整。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">官方网站： https: </font></font><a href="https://freshrss.org" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//freshrss.org</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">演示： https: </font></font><a href="https://demo.freshrss.org" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">//demo.freshrss.org</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">许可证：</font></font><a href="https://www.gnu.org/licenses/agpl-3.0.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GNU AGPL 3</font></font></a></li>
</ul>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/FreshRSS/FreshRSS/blob/edge/docs/img/FreshRSS-logo.png"><img src="/FreshRSS/FreshRSS/raw/edge/docs/img/FreshRSS-logo.png" alt="新鲜RSS标志" style="max-width: 100%;"></a></p>
<h2 tabindex="-1" dir="auto"><a id="user-content-feedback-and-contributions" class="anchor" aria-hidden="true" tabindex="-1" href="#feedback-and-contributions"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">反馈和贡献</font></font></h2>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">欢迎功能请求、错误报告和其他贡献。</font><font style="vertical-align: inherit;">最好的方法是</font></font><a href="https://github.com/FreshRSS/FreshRSS/issues"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 GitHub 上提出问题</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font><font style="vertical-align: inherit;">我们是一个友好的社区。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为了促进贡献，</font><font style="vertical-align: inherit;">可以使用</font></font><a href="/FreshRSS/FreshRSS/blob/edge/.devcontainer/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以下选项：</font></font></a><font style="vertical-align: inherit;"></font></p>
<p dir="auto"><a href="https://github.com/codespaces/new?hide_repo_select=true&amp;ref=edge&amp;repo=6322699"><img src="https://github.com/codespaces/badge.svg" alt="在 GitHub Codespaces 中打开" style="max-width: 100%;"></a></p>
<h2 tabindex="-1" dir="auto"><a id="user-content-screenshot" class="anchor" aria-hidden="true" tabindex="-1" href="#screenshot"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">截屏</font></font></h2>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/FreshRSS/FreshRSS/blob/edge/docs/img/FreshRSS-screenshot.png"><img src="/FreshRSS/FreshRSS/raw/edge/docs/img/FreshRSS-screenshot.png" alt="FreshRSS 截图" style="max-width: 100%;"></a></p>
<h2 tabindex="-1" dir="auto"><a id="user-content-disclaimer" class="anchor" aria-hidden="true" tabindex="-1" href="#disclaimer"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">免责声明</font></font></h2>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FreshRSS 绝对不提供任何保修。</font></font></p>
<h1 tabindex="-1" dir="auto"><a id="user-content-documentation" class="anchor" aria-hidden="true" tabindex="-1" href="#documentation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><a href="https://freshrss.github.io/FreshRSS/en/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a></h1>
<ul dir="auto">
<li><a href="https://freshrss.github.io/FreshRSS/en/users/02_First_steps.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用户文档</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">，您可以在其中发现 FreshRSS 提供的所有可能性</font></font></li>
<li><a href="https://freshrss.github.io/FreshRSS/en/admins/01_Index.html" rel="nofollow"><font style="vertical-align: inherit;"></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关详细安装和维护相关任务的</font><a href="https://freshrss.github.io/FreshRSS/en/admins/01_Index.html" rel="nofollow"><font style="vertical-align: inherit;">管理员文档</font></a></font></li>
<li><a href="https://freshrss.github.io/FreshRSS/en/developers/01_Index.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开发人员文档</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可指导您了解 FreshRSS 的源代码，并在您想做出贡献时为您提供帮助</font></font></li>
<li><a href="https://freshrss.github.io/FreshRSS/en/contributing.html" rel="nofollow"><font style="vertical-align: inherit;"></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为那些想要帮助改进 FreshRSS 的人</font><a href="https://freshrss.github.io/FreshRSS/en/contributing.html" rel="nofollow"><font style="vertical-align: inherit;">提供的贡献者指南</font></a></font></li>
</ul>
<h1 tabindex="-1" dir="auto"><a id="user-content-requirements" class="anchor" aria-hidden="true" tabindex="-1" href="#requirements"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要求</font></font></h1>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最近的浏览器，如 Firefox / IceCat、Edge、Chromium / Chrome、Opera、Safari。
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">适用于移动设备（少数功能除外）</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">运行 Linux 或 Windows 的轻型服务器
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它甚至可以在 Raspberry Pi 1 上运行，响应时间低于一秒（使用 150 个提要、22k 篇文章进行测试）</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Web服务器：Apache2.4+（推荐）、nginx、lighttpd（未在其他平台上测试）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PHP 7.4+
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">所需扩展：</font></font><a href="https://www.php.net/curl" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">cURL</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><a href="https://www.php.net/dom" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DOM</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><a href="https://www.php.net/json" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">JSON</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><a href="https://www.php.net/xml" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">XML</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><a href="https://www.php.net/session" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">session</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><a href="https://www.php.net/ctype" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ctype</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">推荐的扩展名：</font></font><a href="https://www.php.net/pdo-sqlite" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDO_SQLite</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（用于导出/导入）、</font></font><a href="https://www.php.net/gmp" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GMP</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（用于 32 位平台上的 API 访问）、</font></font><a href="https://www.php.net/intl.idn" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">IDN</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（用于国际化域名）、</font></font><a href="https://www.php.net/mbstring" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">mbstring</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（用于 Unicode 字符串）、</font></font><a href="https://www.php.net/iconv" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">iconv</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（用于字符集转换）、</font></font><a href="https://www.php.net/zip" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ZIP</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（用于导入/导出）导出）、</font></font><a href="https://www.php.net/zlib" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">zlib</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（用于压缩源）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据库扩展：</font></font><a href="https://www.php.net/pdo-pgsql" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDO_PGSQL</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或</font></font><a href="https://www.php.net/pdo-sqlite" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDO_SQLite</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或</font></font><a href="https://www.php.net/pdo-mysql" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PDO_MySQL</font></font></a></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PostgreSQL 9.5+ 或 SQLite 或 MySQL 5.5.3+ 或 MariaDB 5.5+</font></font></li>
</ul>
<h1 tabindex="-1" dir="auto"><a id="user-content-installation" class="anchor" aria-hidden="true" tabindex="-1" href="#installation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><a href="https://freshrss.github.io/FreshRSS/en/admins/03_Installation.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></a></h1>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="https://github.com/FreshRSS/FreshRSS/releases/latest"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最新的稳定版本可以在这里</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">找到</font><font style="vertical-align: inherit;">。</font><font style="vertical-align: inherit;">新版本每两到三个月发布一次。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您想要具有最新功能的滚动版本，或者想要帮助测试或开发下一个稳定版本，您可以</font><a href="https://github.com/FreshRSS/FreshRSS/tree/edge/"><font style="vertical-align: inherit;">使用</font></a></font><a href="https://github.com/FreshRSS/FreshRSS/tree/edge/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分支</font></font><code>edge</code><font style="vertical-align: inherit;"></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<h2 tabindex="-1" dir="auto"><a id="user-content-automated-install" class="anchor" aria-hidden="true" tabindex="-1" href="#automated-install"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动安装</font></font></h2>
<ul dir="auto">
<li><a href="/FreshRSS/FreshRSS/blob/edge/Docker"><img src="https://camo.githubusercontent.com/ec03ea85273ec749baa6c4d64546c278124d567763520968bad595644f3db3a6/68747470733a2f2f7777772e646f636b65722e636f6d2f77702d636f6e74656e742f75706c6f6164732f323032322f30332f686f72697a6f6e74616c2d6c6f676f2d6d6f6e6f6368726f6d617469632d77686974652e706e67" width="200" alt="码头工人" data-canonical-src="https://www.docker.com/wp-content/uploads/2022/03/horizontal-logo-monochromatic-white.png" style="max-width: 100%;"></a></li>
<li><a href="https://install-app.yunohost.org/?app=freshrss" rel="nofollow"><img src="https://camo.githubusercontent.com/be23bd8c4c66742c8899c65d66d378e1b49217cef8825801a7947832e04a831f/68747470733a2f2f696e7374616c6c2d6170702e79756e6f686f73742e6f72672f696e7374616c6c2d776974682d79756e6f686f73742e706e67" alt="尤诺主机" data-canonical-src="https://install-app.yunohost.org/install-with-yunohost.png" style="max-width: 100%;"></a></li>
<li><a href="https://cloudron.io/button.html?app=org.freshrss.cloudronapp" rel="nofollow"><img src="https://camo.githubusercontent.com/2090447e42db7c78b7c0ef04dde56ff95840ba25bbbe1bc9b342e718fcf97638/68747470733a2f2f636c6f7564726f6e2e696f2f696d672f627574746f6e2e737667" alt="云龙" data-canonical-src="https://cloudron.io/img/button.svg" style="max-width: 100%;"></a></li>
<li><a href="https://www.pikapods.com/pods?run=freshrss" rel="nofollow"><img src="https://camo.githubusercontent.com/16cf45230a7b0954fa2fe0ee8a9c33801ea7568ffed76baf1e7b83902535579e/68747470733a2f2f7777772e70696b61706f64732e636f6d2f7374617469632f72756e2d627574746f6e2d33342e737667" alt="鼠兔豆荚" data-canonical-src="https://www.pikapods.com/static/run-button-34.svg" style="max-width: 100%;"></a></li>
</ul>
<h2 tabindex="-1" dir="auto"><a id="user-content-manual-install" class="anchor" aria-hidden="true" tabindex="-1" href="#manual-install"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">手动安装</font></font></h2>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 git 或</font></font><a href="https://github.com/FreshRSS/FreshRSS/archive/latest.zip"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下载存档获取 FreshRSS</font></font></a></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将应用程序放在服务器上的某个位置（仅</font></font><code>./p/</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">向 Web 公开文件夹）</font></font></li>
<li><font style="vertical-align: inherit;"></font><code>./data/</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为网络服务器用户</font><font style="vertical-align: inherit;">添加对文件夹的写入权限</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用浏览器访问 FreshRSS 并按照安装过程进行操作
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或使用</font></font><a href="/FreshRSS/FreshRSS/blob/edge/cli/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">命令行界面</font></font></a></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一切都应该正常:) 如果您遇到任何问题，请随时与</font></font><a href="https://github.com/FreshRSS/FreshRSS/issues"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们联系</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></li>
<li><font style="vertical-align: inherit;"></font><a href="/FreshRSS/FreshRSS/blob/edge/config.default.php"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高级配置设置可以在config.default.php</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中找到</font><font style="vertical-align: inherit;">并在 中修改</font></font><code>data/config.php</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Apache 时，启用</font></font><a href="https://httpd.apache.org/docs/trunk/mod/core.html#allowencodedslashes" rel="nofollow"><code>AllowEncodedSlashes</code></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与移动客户端的更好兼容性。</font></font></li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="https://freshrss.github.io/FreshRSS/en/admins/03_Installation.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关安装和服务器配置的更多详细信息可以在我们的文档</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中找到</font><font style="vertical-align: inherit;">。</font></font></p>
<h1 tabindex="-1" dir="auto"><a id="user-content-advice" class="anchor" aria-hidden="true" tabindex="-1" href="#advice"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">建议</font></font></h1>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为了获得更好的安全性，请仅</font></font><code>./p/</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">向 Web 公开该文件夹。
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请注意，该</font></font><code>./data/</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件夹包含所有个人数据，因此公开它是一个坏主意。</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该</font></font><code>./constants.php</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件定义对应用程序文件夹的访问。</font><font style="vertical-align: inherit;">如果您想自定义安装，请先查看此处。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果遇到任何问题，可以从界面或在</font></font><code>./data/users/*/log*.txt</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件中手动访问日志。
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特殊文件夹</font></font><code>./data/users/_/</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">包含所有用户共享的日志部分。</font></font></li>
</ul>
</li>
</ul>
<h1 tabindex="-1" dir="auto"><a id="user-content-faq" class="anchor" aria-hidden="true" tabindex="-1" href="#faq"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">常问问题</font></font></h1>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">右栏中的日期和时间是提要声明的日期，而不是 FreshRSS 收到文章的时间，并且不用于排序。
</font></font><ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特别是，当导入新的 feed 时，其所有文章都将显示在 feed 列表的顶部，无论其声明日期如何。</font></font></li>
</ul>
</li>
</ul>
<h1 tabindex="-1" dir="auto"><a id="user-content-extensions" class="anchor" aria-hidden="true" tabindex="-1" href="#extensions"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">扩展</font></font></h1>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FreshRSS 通过在其核心功能之上添加扩展来支持进一步的定制。</font><font style="vertical-align: inherit;">请参阅</font></font><a href="https://github.com/FreshRSS/Extensions"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">专用于这些扩展的存储库</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<h1 tabindex="-1" dir="auto"><a id="user-content-apis--native-apps" class="anchor" aria-hidden="true" tabindex="-1" href="#apis--native-apps"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">API 和本机应用程序</font></font></h1>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FreshRSS 支持通过两个不同的 API 从适用于 Linux、Android、iOS、Windows 和 macOS 的移动/本机应用程序进行访问：
 </font></font><a href="https://freshrss.github.io/FreshRSS/en/users/06_Mobile_access.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Google Reader API</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（最佳）和</font></font><a href="https://freshrss.github.io/FreshRSS/en/users/06_Fever_API.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fever API</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（功能有限且效率较低）。</font></font></p>
<table>
<thead>
<tr>
<th align="left"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">应用程序</font></font></th>
<th align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">平台</font></font></th>
<th align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自由软件</font></font></th>
<th align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">维护和开发</font></font></th>
<th align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">应用程序编程接口</font></font></th>
<th align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">离线工作</font></font></th>
<th align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速同步</font></font></th>
<th align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在个人视图中获取更多内容</font></font></th>
<th align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">获取已读文章</font></font></th>
<th align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">收藏夹</font></font></th>
<th align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">标签</font></font></th>
<th align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">播客</font></font></th>
<th align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">管理提要</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td align="left"><a href="https://github.com/noinnion/newsplus/blob/master/apk/NewsPlus_202.apk"><font style="vertical-align: inherit;"></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">带有</font><a href="https://github.com/noinnion/newsplus/blob/master/apk/GoogleReaderCloneExtension_101.apk"><font style="vertical-align: inherit;">Google 阅读器扩展的</font></a><a href="https://github.com/noinnion/newsplus/blob/master/apk/NewsPlus_202.apk"><font style="vertical-align: inherit;">News+</font></a></font><a href="https://github.com/noinnion/newsplus/blob/master/apk/GoogleReaderCloneExtension_101.apk"><font style="vertical-align: inherit;"></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安卓</font></font></td>
<td align="center"><a href="https://github.com/noinnion/newsplus/blob/master/extensions/GoogleReaderCloneExtension/src/com/noinnion/android/newsplus/extension/google_reader/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">部分</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2015年</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读器</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐⭐⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://play.google.com/store/apps/details?id=com.seazon.feedme" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">喂我</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">*</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安卓</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读器</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✓</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://github.com/Alkarex/EasyRSS"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">简易RSS</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安卓</font></font></td>
<td align="center"><a href="https://github.com/Alkarex/EasyRSS"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读器</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">漏洞</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://github.com/readrops/Readrops"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">读滴</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安卓</font></font></td>
<td align="center"><a href="https://github.com/readrops/Readrops"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读器</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐⭐⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://hyliu.me/fluent-reader-lite/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fluent Reader精简版</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安卓、iOS</font></font></td>
<td align="center"><a href="https://github.com/yang991178/fluent-reader-lite"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">G阅读器，发烧</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐⭐⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✓</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://play.google.com/store/apps/details?id=allen.town.focus.reader" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">焦点阅读器</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安卓</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读器</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐⭐⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✓</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://github.com/Ashinch/ReadYou/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">读你</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安卓</font></font></td>
<td align="center"><a href="https://github.com/Ashinch/ReadYou/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></a></td>
<td align="center"><a href="https://github.com/Ashinch/ReadYou/discussions/542" data-hovercard-type="discussion" data-hovercard-url="/Ashinch/ReadYou/discussions/542/hovercard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工作正在进行中</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">G阅读器，发烧</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://gitlab.com/christophehenry/freshrss-android" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">克里斯托夫·亨利</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安卓</font></font></td>
<td align="center"><a href="https://gitlab.com/christophehenry/freshrss-android" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工作正在进行中</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读器</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://hyliu.me/fluent-reader/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">流利的阅读器</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Windows、Linux、macOS</font></font></td>
<td align="center"><a href="https://github.com/yang991178/fluent-reader"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">G阅读器，发烧</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✓</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://github.com/martinrotter/rssguard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RSS 卫士</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Windows、GNU/Linux、macOS、OS/2</font></font></td>
<td align="center"><a href="https://github.com/martinrotter/rssguard"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读器</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://gitlab.com/news-flash/news_flash_gtk" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新闻快报</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GNU/Linux</font></font></td>
<td align="center"><a href="https://gitlab.com/news-flash/news_flash_gtk" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">G阅读器，发烧</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://newsboat.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新闻船 2.24+</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GNU/Linux、macOS、FreeBSD</font></font></td>
<td align="center"><a href="https://github.com/newsboat/newsboat/"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读器</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
</tr>
<tr>
<td align="left"><a href="http://www.vienna-rss.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">维也纳RSS</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">苹果系统</font></font></td>
<td align="center"><a href="https://github.com/ViennaRSS/vienna-rss"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读器</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://apps.apple.com/app/readkit-read-later-rss/id1615798039" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读套件</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">iOS、macOS</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读器</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐⭐⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✓</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">💲</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://www.reederapp.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">里德</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">*</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">iOS、macOS</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">G阅读器，发烧</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⭐⭐⭐</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://lireapp.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">里拉</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">iOS、macOS</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读器</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://apps.apple.com/app/unread-2/id1363637349" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">未读</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">iOS系统</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发烧</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://apps.apple.com/app/fiery-feeds-rss-reader/id1158763303" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">火热饲料</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">iOS系统</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发烧</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
</tr>
<tr>
<td align="left"><a href="https://ranchero.com/netnewswire/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网络通讯社</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">iOS、macOS</font></font></td>
<td align="center"><a href="https://github.com/Ranchero-Software/NetNewsWire"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></a></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工作正在进行中</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阅读器</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">➖</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">❔</font></font></td>
<td align="center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✔️</font></font></td>
</tr>
</tbody>
</table>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">* 如果您使用的是 Reeder 4 或 FeedMe，请安装并启用</font></font><a href="https://github.com/javerous/freshrss-greader-redate"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GReader Redate 扩展</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以获得提要文章的正确发布日期。</font><font style="vertical-align: inherit;">（Reeder 5 不再需要）</font></font></p>
<h1 tabindex="-1" dir="auto"><a id="user-content-included-libraries" class="anchor" aria-hidden="true" tabindex="-1" href="#included-libraries"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">包含的库</font></font></h1>
<ul dir="auto">
<li><a href="https://simplepie.org/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">简单饼图</font></font></a></li>
<li><a href="https://framagit.org/marienfressinaud/MINZ" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">明兹</font></font></a></li>
<li><a href="https://alexandre.alapetite.fr/doc-alex/php-http-304/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">php-http-304</font></font></a></li>
<li><a href="https://framagit.org/marienfressinaud/lib_opml" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">lib_opml</font></font></a></li>
<li><a href="https://github.com/PhpGt/CssXPath"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PhpGt/CssXPath</font></font></a></li>
<li><a href="https://github.com/PHPMailer/PHPMailer"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PHP邮件程序</font></font></a></li>
<li><a href="https://www.chartjs.org" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Chart.js</font></font></a></li>
</ul>
<h2 tabindex="-1" dir="auto"><a id="user-content-only-for-some-options-or-configurations" class="anchor" aria-hidden="true" tabindex="-1" href="#only-for-some-options-or-configurations"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">仅适用于某些选项或配置</font></font></h2>
<ul dir="auto">
<li><a href="https://github.com/dcodeIO/bcrypt.js"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">bcrypt.js</font></font></a></li>
<li><a href="https://github.com/phpquery/phpquery"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">php查询</font></font></a></li>
</ul>
<h1 tabindex="-1" dir="auto"><a id="user-content-alternatives" class="anchor" aria-hidden="true" tabindex="-1" href="#alternatives"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">备择方案</font></font></h1>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果 FreshRSS 由于某种原因不适合您，可以考虑以下替代解决方案：</font></font></p>
<ul dir="auto">
<li><a href="https://tontof.net/kriss/feed/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">克里斯饲料</font></font></a></li>
<li><a href="https://github.com/LeedRSS/Leed"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">利德</font></font></a></li>
<li><a href="https://alternativeto.net/software/freshrss/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">还有更多……</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">（但如果您喜欢 FreshRSS，请给我们投票！）</font></font></li>
</ul>
</article></div>
